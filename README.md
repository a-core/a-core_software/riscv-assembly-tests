# RISC-V Assembly Test Programs
This repository hosts assembly test programs for A-Core. To add a new program,
copy the directory `src/riscv-c-template` to a new directory under `src/` and
rename it appropriately. The set of enabled hardware features can be
controlled with the `-march` and `-mabi` flags in the corresponding `Makefile`.

## Repository Structure
* `README.md` -- You are reading it right now.
* `LICENSE_AALTO.txt` -- Software license.
* `init_submodules.sh` -- Helper script for recursively cloning git submodules.
* `src/` -- Source directory for all different test programs.
* `src/riscv-assembly-template` -- A minimal assembly program template for A-Core.

## Programs
* `m-tests` -- Program for testing M extension features.
* `f-tests` -- Program for testing F extension features.
* `gpio-tests` -- Program for testing memory mapped GPIO.

## Compiling Programs
All programs can be compiled by executing:
```shell
$ ./configure && make
```

Individual programs can be compiled with `make` in the appropriate source directory.