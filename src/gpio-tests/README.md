# GPIO Tests
This is a small assembly script for testing GPIO.

## Repository Structure
* README.md -- you are reading it right now
* a-core.ld -- [linker script](https://sourceware.org/binutils/docs/ld/Scripts.html) for the A-Core target
* main.s -- main assembly source file

## Linking
The linking process is controlled by the linker script `a-core.ld` which contains configurations for binaries
targeting A-Core. For reference, the default RISC-V linker script can be viewed with `riscv64-unknown-elf-ld --verbose`.

## Compilation Instructions
The program can be built with `make` which produces the binary file `main.elf`.
