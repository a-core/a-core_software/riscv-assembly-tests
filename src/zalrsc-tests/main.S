# Tests for for LR.W and SC.W atomic memory accesses from Zalrsc extension (part of A extension)
# These tests assume that the core is configured with a data cache with 4 words per cache line.

#include "test_macros.h"

init_test_data:
    saveContext

    # first cache line
    li      t0, 0x20000000
    li      t1, 0x00000000
    sw      t1, 0(t0)
    li      t0, 0x20000004
    li      t1, 0x11111111
    sw      t1, 0(t0)
    li      t0, 0x20000008
    li      t1, 0x22222222
    sw      t1, 0(t0)
    li      t0, 0x2000000c
    li      t1, 0x33333333
    sw      t1, 0(t0)

    # second cache line
    li      t0, 0x20000010
    li      t1, 0x44444444
    sw      t1, 0(t0)
    li      t0, 0x20000014
    li      t1, 0x55555555
    sw      t1, 0(t0)
    loadContext
    ret

init

# LR.W loads correct value from memory
test_0_lrw:
    li      TESTNUM, 0
    call init_test_data

    # load cache line
    li      t0, 0x20000004
    lw      a1, 0(t0)

    # load all entries in cache line in succession
    li      t0, 0x20000000
    lr.w    a1, (t0)
    li      t0, 0x20000004
    lr.w    a1, (t0)
    li      t0, 0x20000008
    lr.w    a1, (t0)
    li      t0, 0x2000000c
    lr.w    a1, (t0)

    # load value from another cache line
    li      t0, 0x20000010
    lr.w    a2, (t0)

    # check last loaded entry
    li      a0, 0x33333333
    bne     a0, a1, err_trap_handler
    call print_ok

# SC.W should write value to memory when paired with LR.W to same addr
test_1_scw_1:
    li      TESTNUM, 1
    call init_test_data

    # atomic xor of DM[0x20000008] = 0x22222222 => 0xDDDDDDDD
    li      t0, 0x20000008
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF
    sc.w    a1, a1, (t0)

    # status in a1 should be zero
    li      a0, 0
    bne     a0, a1, err_trap_handler

    # value in memory should be 0xDDDDDDDD
    li      a0, 0xDDDDDDDD
    lw      a1, 0(t0)
    bne     a0, a1, err_trap_handler

    call print_ok

# SC.W should not write value to memory when paired with LR.W to diff addr (same cache line)
test_2_scw_2:
    li      TESTNUM, 2
    call init_test_data

    # load reserved from 0x20000008
    # store conditional to 0x2000000C
    # conditional store to different address should fail
    li      t0, 0x20000008
    li      t1, 0x2000000C
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF
    sc.w    a1, a1, (t1)

    # status in a1 should be nonzero
    li      a0, 0
    beq     a0, a1, err_trap_handler

    # value at address 0x2000000C should still be 0x33333333
    li      a0, 0x33333333
    lw      a1, 0(t1)
    bne     a0, a1, err_trap_handler

    call print_ok

# SC.W should not write value to memory when paired with LR.W to diff addr (different cache line)
test_3_scw_3:
    li      TESTNUM, 3
    call init_test_data

    # load reserved from 0x20000008
    # store conditional to 0x20000010
    # conditional store to different address should fail
    li      t0, 0x20000008
    li      t1, 0x20000010
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF
    sc.w    a1, a1, (t1)

    # status in a1 should be nonzero
    li      a0, 0
    beq     a0, a1, err_trap_handler

    # value at address 0x20000010 should still be 0x44444444
    li      a0, 0x44444444
    lw      a1, 0(t1)
    bne     a0, a1, err_trap_handler

    call print_ok

# test_scw_4: SC.W should not write value to memory when not paired with any LR.W
test_4_scw_4:
    li      TESTNUM, 4
    call init_test_data

    # store conditional to 0x20000014, not paired with any previous LR.W, should fail
    li      t1, 0x20000014
    sc.w    a1, a1, (t1)

    # status in a1 should be nonzero
    li      a0, 0
    beq     a0, a1, err_trap_handler

    # value at address 0x20000014 should still be 0x55555555
    li      a0, 0x55555555
    lw      a1, 0(t1)
    bne     a0, a1, err_trap_handler

    call print_ok

# test_scw_5: Normal store to same address should clear reservation (SC.W fails)
test_5_scw_5:
    li      TESTNUM, 5
    call init_test_data

    # load reserved from 0x20000004
    # conditional store to different address should fail due to normal store to same addr
    li      t0, 0x20000004
    li      a2, 0x49234629 # random garbage

    # start atomic access
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF

    # oh no somebody writes garbage to 0x20000004 between LR.W and SC.W
    sw      a2, 0(t0)

    sc.w    a1, a1, (t0)

    # status in a1 should be nonzero
    li      a0, 0
    beq     a0, a1, err_trap_handler

    # value at address 0x20000004 should be written garbage
    lw      a1, 0(t0)
    bne     a1, a2, err_trap_handler

    call print_ok

# test_scw_6: Normal store to another addr should not clear reservation (SC.W succeeds)
test_6_scw_6:
    li      TESTNUM, 6
    call init_test_data

    li      t0, 0x20000004
    li      t1, 0x20000008
    li      a2, 0x49234629 # random garbage

    # start atomic xor of DM[0x20000008] = 0x11111111 => 0xEEEEEEEE
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF

    # hmm, somebody writes garbage to the same cache line between LR.W and SC.W
    sw      a2, 0(t1)

    # conditional store should still succeed
    sc.w    a1, a1, (t0)

    # status in a1 should be zero
    li      a0, 0
    bne     a0, a1, err_trap_handler

    # value at address 0x20000004 should be 0xEEEEEEEE
    li      a0, 0xEEEEEEEE
    lw      a1, 0(t0)
    bne     a0, a1, err_trap_handler

    call print_ok

# test_scw_7: Normal load from same address should not clear reservation (SC.W succeeds)
test_7_scw_7:
    li      TESTNUM, 7
    call init_test_data

    li      t0, 0x20000004

    # start atomic xor of DM[0x20000008] = 0x11111111 => 0xEEEEEEEE
    lr.w    a1, (t0)
    xori    a1, a1, 0xFFFFFFFF

    # hmm, somebody loads from reserved address, this should not affect reservation
    lw      a2, 0(t0)

    # conditional store should still succeed
    sc.w    a1, a1, (t0)

    # status in a1 should be zero
    li      a0, 0
    bne     a0, a1, err_trap_handler

    # value at address 0x20000004 should be 0xEEEEEEEE
    li      a0, 0xEEEEEEEE
    lw      a1, 0(t0)
    bne     a0, a1, err_trap_handler

    call print_ok

j test_pass
