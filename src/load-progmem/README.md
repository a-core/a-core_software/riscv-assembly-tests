# RISC-V Assembly Program template
This repository contains an example configuration of a RV32I assembly program executing on the A-Core processor.

## Repository Structure
* README.md -- you are reading it right now
* Makefile -- build system configuration for GNU make
* a-core.ld -- [linker script](https://sourceware.org/binutils/docs/ld/Scripts.html) for the A-Core target
* main.s -- Main assembly program source

## Linking
The linking process is controlled by the linker script `a-core.ld` which contains configurations for binaries
targeting A-Core. For reference, the default RISC-V linker script can be viewed with `riscv64-unknown-elf-ld --verbose`.

## Compilation Instructions
The program can be built with `./make` which produces the binary file `main.elf`.
